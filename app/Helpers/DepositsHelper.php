<?php

namespace App\Helpers;

use App\Jobs\EmailAccountCreditJob;
use App\Ledger;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class DepositsHelper
{

    public function depositFunds($request)
    {

        $ledgerHelper = new LedgerHelper();
        $balance = $ledgerHelper->getBalance();

        $balance = $balance + $request->amount;

        $request->request->add(['date_time' => Carbon::now()->format(config('app.date_format') . ' H:i:s')]);
        $request->request->add(['type' => 'Credit']);
        $request->request->add(['details' => 'Deposit']);
        $request->request->add(['balance' => $balance]);
        $request->request->add(['created_by_id' => Auth::user()->id]);
        $request->request->add(['to_name' => Auth::user()->name]);
        $request->request->add(['to_email' => Auth::user()->email]);

        $deposit = Ledger::create($request->all());

        $toEmailAddressList = [Auth::user()->email];

        $delayBySeconds = 1;
        $job = (new EmailAccountCreditJob($request,$toEmailAddressList))->delay(Carbon::now()->addSeconds($delayBySeconds));
        dispatch($job);

        $message = $this->newDepositMessage($deposit);
        return $message;

    }


    public function newDepositMessage($deposit)
    {
        $message = 'Rs. ' . $deposit->amount . ' is credited into your account. Your a/c balance is ' . $deposit->balance . '.';
        return $message;
    }


}
