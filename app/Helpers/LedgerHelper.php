<?php

namespace App\Helpers;

use App\Ledger;
use Illuminate\Support\Facades\Auth;

class LedgerHelper
{

    public function getBalance($ledgerCreatorId = null)
    {
        if ($ledgerCreatorId == null) {
            $ledgerCreatorId = Auth::user()->id;
        }

        $latest_record = Ledger::where('created_by_id', $ledgerCreatorId)->orderBy('created_at',
            'desc')->select(['balance'])->first();

        if ($latest_record == null) {
            $balance = 0;
        } else {
            $balance = $latest_record->balance;
        }

        return $balance;

    }


}
