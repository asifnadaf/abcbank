<?php

namespace App\Helpers;

use App\Jobs\EmailAccountCreditJob;
use App\Jobs\EmailAccountDebitJob;
use App\Ledger;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;

class TransfersHelper
{


    public function transferFunds($request)
    {

        $message = null;

        DB::beginTransaction();
        try {
            $debit_response = $this->withdrawFunds($request);
            $credit_response = $this->depositFunds($request);
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
        }

        $message = $this->newTransferMessage($debit_response);
        return $message;
    }


    public function withdrawFunds($request)
    {
        $ledgerHelper = new LedgerHelper();
        $balance = $ledgerHelper->getBalance();

        $balance = $balance - $request->amount;

        $request->request->add(['date_time' => Carbon::now()->format(config('app.date_format') . ' H:i:s')]);
        $request->request->add(['type' => 'Debit']);
        $request->request->add(['details' => 'Transfer to ' . $request->email]);
        $request->request->add(['balance' => $balance]);
        $request->request->add(['created_by_id' => Auth::user()->id]);
        $request->request->add(['to_name' => Auth::user()->name]);
        $request->request->add(['to_email' => Auth::user()->email]);

        $debit_response = Ledger::create($request->all());

        $toEmailAddressList = [Auth::user()->email];
        $delayBySeconds = 1;
        $job = (new EmailAccountDebitJob($request,$toEmailAddressList))->delay(Carbon::now()->addSeconds($delayBySeconds));
        dispatch($job);


        return $debit_response;

    }

    public function depositFunds($request)
    {
        $beneficiary = User::where('email', $request->email)->first();

        $ledgerHelper = new LedgerHelper();
        $balance = $ledgerHelper->getBalance($beneficiary->id);

        $balance = $balance + $request->amount;

        $credit_request = $request;

        $credit_request->request->add(['date_time' => Carbon::now()->format(config('app.date_format') . ' H:i:s')]);
        $credit_request->request->add(['type' => 'Credit']);
        $credit_request->request->add(['details' => 'Transfer from ' . Auth::user()->email]);
        $credit_request->request->add(['amount' => $request->amount]);
        $credit_request->request->add(['balance' => $balance]);
        $credit_request->request->add(['created_by_id' => $beneficiary->id]);

        $request->request->add(['to_name' => $beneficiary->name]);
        $request->request->add(['to_email' => $beneficiary->email]);

        $credit_response = Ledger::create($credit_request->all());

        $toEmailAddressList = [$beneficiary->email];
        $delayBySeconds = 1;
        $job = (new EmailAccountCreditJob($request,$toEmailAddressList))->delay(Carbon::now()->addSeconds($delayBySeconds));
        dispatch($job);

        return $credit_response;

    }


    public function newTransferMessage($request)
    {
        $message = 'Rs. ' . $request->amount . ' is debited from your account & credited to ' . $request->email . ' Your a/c balance is ' . ($request->balance - $request->amount) . '.';
        return $message;
    }


}
