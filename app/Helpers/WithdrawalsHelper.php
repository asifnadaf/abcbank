<?php

namespace App\Helpers;

use App\Jobs\EmailAccountDebitJob;
use App\Ledger;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class WithdrawalsHelper
{

    public function withdrawFunds($request)
    {
        $ledgerHelper = new LedgerHelper();
        $balance = $ledgerHelper->getBalance();

        $balance = $balance - $request->amount;

        $request->request->add(['date_time' => Carbon::now()->format(config('app.date_format') . ' H:i:s')]);
        $request->request->add(['type' => 'Debit']);
        $request->request->add(['details' => 'withdraw']);
        $request->request->add(['balance' => $balance]);
        $request->request->add(['created_by_id' => Auth::user()->id]);
        $request->request->add(['to_name' => Auth::user()->name]);
        $request->request->add(['to_email' => Auth::user()->email]);

        $withdrawal = Ledger::create($request->all());

        $toEmailAddressList = [Auth::user()->email];

        $delayBySeconds = 1;
        $job = (new EmailAccountDebitJob($request,$toEmailAddressList))->delay(Carbon::now()->addSeconds($delayBySeconds));
        dispatch($job);


        $message = $this->newWithdrawalMessage($withdrawal);
        return $message;

    }


    public function newWithdrawalMessage($withdrawal)
    {
        $message = 'Rs. ' . $withdrawal->amount . ' is debited from your account. Your a/c balance is ' . $withdrawal->balance . '.';
        return $message;
    }


}
