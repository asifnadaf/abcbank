<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\DepositsHelper;
use App\Helpers\LedgerHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreDepositsRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class DepositsController extends Controller
{

    /**
     * Show the form for creating new Deposit.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('deposit_create')) {
            return abort(401);
        }

        $ledgerHelper = new LedgerHelper();
        $currentBalance = $ledgerHelper->getBalance();

        return view('admin.deposits.create', compact('currentBalance'));
    }

    /**
     * Store a newly created ledger in storage.
     *
     * @param  \App\Http\Requests\StoreledgersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDepositsRequest $request)
    {

        if (!Gate::allows('deposit_create')) {
            return abort(401);
        }

        $depositsHelper = new DepositsHelper();
        $message = $depositsHelper->depositFunds($request);

        return redirect()->route('admin.deposits.create')->with('message', $message);
    }

}
