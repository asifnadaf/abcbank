<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreLedgersRequest;
use App\Http\Requests\Admin\UpdateLedgersRequest;
use App\Ledger;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class LedgersController extends Controller
{
    /**
     * Display a listing of Ledger.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('admin.ledgers.index', compact('users'));
    }


    /**
     * Display Ledger.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (!Gate::allows('ledger_view')) {
            return abort(401);
        }
        $ledgers = \App\Ledger::where('created_by_id', $id)->get();

        $user = User::findOrFail($id);

        return view('admin.ledgers.show', compact('user', 'ledgers'));
    }

}
