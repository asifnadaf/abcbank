<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Ledger;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class StatementsController extends Controller
{
    /**
     * Display Ledger statement.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Gate::allows('statement_access')) {
            return abort(401);
        }
        if ($filterBy = Input::get('filter')) {
            if ($filterBy == 'all') {
                Session::put('ledger.filter', 'all');
            } elseif ($filterBy == 'my') {
                Session::put('ledger.filter', 'my');
            }
        }

        $ledgers = Ledger::where('created_by_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('admin.statements.index', compact('ledgers'));
    }


}
