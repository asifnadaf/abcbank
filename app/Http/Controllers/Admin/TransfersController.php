<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\LedgerHelper;
use App\Helpers\TransfersHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreTransfersRequest;
use App\Rules\IsBeneficiaryEmailAddressExist;
use App\Rules\IsEmailAddressSame;
use App\Rules\IsEnoughBalance;
use Illuminate\Support\Facades\Gate;

class TransfersController extends Controller
{

    /**
     * Show the form for creating new withdrawal.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('transfer_create')) {
            return abort(401);
        }

        $ledgerHelper = new LedgerHelper();
        $currentBalance = $ledgerHelper->getBalance();

        return view('admin.transfers.create', compact('currentBalance'));
    }

    /**
     * Store a newly created ledger in storage.
     *
     * @param  \App\Http\Requests\StoreledgersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransfersRequest $request)
    {
        if (!Gate::allows('transfer_create')) {
            return abort(401);
        }

        $this->validate($request, ['email' => new IsBeneficiaryEmailAddressExist()]);
        $this->validate($request, ['email' => new IsEmailAddressSame()]);
        $this->validate($request, ['amount' => new IsEnoughBalance()]);

        $transfersHelper = new TransfersHelper();
        $message = $transfersHelper->transferFunds($request);

        return redirect()->route('admin.transfers.create')->with('message', $message);
    }

}
