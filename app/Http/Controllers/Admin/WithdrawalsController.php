<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\LedgerHelper;
use App\Helpers\WithdrawalsHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreWithdrawalsRequest;
use App\Ledger;
use App\Rules\IsEnoughBalance;
use Illuminate\Support\Facades\Gate;

class WithdrawalsController extends Controller
{

    /**
     * Show the form for creating new withdrawal.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::allows('withdraw_create')) {
            return abort(401);
        }

        $ledgerHelper = new LedgerHelper();
        $currentBalance = $ledgerHelper->getBalance();

        return view('admin.withdrawals.create', compact('currentBalance'));
    }

    /**
     * Store a newly created ledger in storage.
     *
     * @param  \App\Http\Requests\StoreledgersRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWithdrawalsRequest $request)
    {
        if (!Gate::allows('withdraw_create')) {
            return abort(401);
        }

        $this->validate($request, ['amount' => new IsEnoughBalance()]);

        $withdrawalsHelper = new WithdrawalsHelper();

        $message = $withdrawalsHelper->withdrawFunds($request);

        return redirect()->route('admin.withdrawals.create')->with('message', $message);
    }

}
