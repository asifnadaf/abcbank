<?php

namespace App\Http\Controllers;

use App\Helpers\LedgerHelper;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $ledgerHelper = new LedgerHelper();
        $currentBalance = $ledgerHelper->getBalance();

        return view('home', compact('currentBalance'));

    }
}
