<?php

namespace App\Jobs;

use App\Mail\EmailAccountDebit;
use Illuminate\Bus\Queueable;
use Illuminate\Http\Request;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class EmailAccountDebitJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3; // Max tries
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $request;
    public $toEmailAddressList;

    public function __construct(Request $request,$toEmailAddressList)
    {
        $this->request = $request->all();
        $this->toEmailAddressList = $toEmailAddressList;
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $emailAccountDebit = new EmailAccountDebit($this->request,$this->toEmailAddressList);
        Mail::to($this->toEmailAddressList)->send($emailAccountDebit);
    }
}
