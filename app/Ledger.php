<?php

namespace App;

use App\Traits\FilterByUser;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ledger
 *
 * @package App
 * @property string $date_time
 * @property decimal $amount
 * @property string $type
 * @property string $details
 * @property string $created_by
 * @property decimal $balance
 */
class Ledger extends Model
{

    protected $fillable = ['date_time', 'amount', 'type', 'details', 'balance', 'created_by_id'];
    protected $hidden = [];


    /**
     * Set attribute to date format
     * @param $input
     */
    public function setDateTimeAttribute($input)
    {
        if ($input != null && $input != '') {
            $this->attributes['date_time'] = Carbon::createFromFormat(config('app.date_format') . ' H:i:s',
                $input)->format('Y-m-d H:i:s');
        } else {
            $this->attributes['date_time'] = null;
        }
    }

    /**
     * Get attribute from date format
     * @param $input
     *
     * @return string
     */
    public function getDateTimeAttribute($input)
    {
        $zeroDate = str_replace(['Y', 'm', 'd'], ['0000', '00', '00'], config('app.date_format') . ' H:i:s');

        if ($input != $zeroDate && $input != null) {
            return Carbon::createFromFormat('Y-m-d H:i:s', $input)->format(config('app.date_format') . ' H:i:s');
        } else {
            return '';
        }
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setAmountAttribute($input)
    {
        $this->attributes['amount'] = $input ? $input : null;
    }

    /**
     * Set to null if empty
     * @param $input
     */
    public function setCreatedByIdAttribute($input)
    {
        $this->attributes['created_by_id'] = $input ? $input : null;
    }

    /**
     * Set attribute to money format
     * @param $input
     */
    public function setBalanceAttribute($input)
    {
        $this->attributes['balance'] = $input ? $input : null;
    }

    public function created_by()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }

}
