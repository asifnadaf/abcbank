<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class EmailAccountDebit extends Mailable
{
    use Queueable, SerializesModels;

    public $data;
    public $toEmailAddressList;

    public function __construct($data,$toEmailAddressList)
    {
        $this->data = $data;
        $this->toEmailAddressList = $toEmailAddressList;
    }

    public function build()
    {
        $fromName = config('mail.from.name');
        $fromEmailAddress = array(config('mail.from.address'));
        $toEmailAddressList = $this->toEmailAddressList;
        $subject = 'Your account is debited for INR '.$this->data['amount'] ;

        return $this->view('emails.account_debit_template')
            ->from($fromEmailAddress)
            ->to($toEmailAddressList)
            ->subject($subject)
            ->with(['data' => $this->data]);
    }
}