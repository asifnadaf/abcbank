<?php

namespace App\Rules;

use App\Helpers\LedgerHelper;
use App\Ledger;
use Illuminate\Contracts\Validation\Rule;

class IsEnoughBalance implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $latest_record = Ledger::orderBy('created_at', 'desc')->select(['balance'])->first();
        $balance = $latest_record->balance;

        if ($balance == null or $balance == '' or $balance == 0) {
            return false;
        }

        if ($value > $balance) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $ledgerHelper = new LedgerHelper();
        $currentBalance = $ledgerHelper->getBalance();
        return 'Your your a/c has insufficient balance of Rs. ' . $currentBalance;
    }
}
