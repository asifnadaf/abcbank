<?php

namespace App;

use Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    protected $fillable = ['name', 'email', 'password', 'remember_token'];
    protected $hidden = ['password', 'remember_token'];


    /**
     * Hash password
     * @param $input
     */
    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }


    public function role()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }


    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function getUserNameInitialsAttribute()
    {

        $fullName = $this->name;

        $words = explode(" ", $fullName);
        $firstCharacter = $words[0][0];

        $lastCharacter = "";
        $counter = 0;
        foreach ($words as $w) {

            if ($counter == 0) {
                $counter++;
                continue;
            }
            $lastCharacter = $w[0][0];
        }

        $nameInitial = $firstCharacter . $lastCharacter;

        return $nameInitial;
    }
}
