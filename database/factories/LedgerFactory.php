<?php

$factory->define(App\Ledger::class, function (Faker\Generator $faker) {
    return [
        "date_time" => $faker->date("d-m-Y H:i:s", $max = 'now'),
        "amount" => $faker->randomNumber(2),
        "type" => $faker->name,
        "details" => $faker->name,
        "created_by_id" => factory('App\User')->create(),
        "balance" => $faker->randomNumber(2),
    ];
});
