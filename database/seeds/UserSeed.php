<?php

use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [

            [
                'id' => 1,
                'name' => 'Deepak Parekh',
                'email' => 'asif@nectotech.com',
                'password' => '$2y$10$ttTVfH/9qGzN0UlN5SRQqu2d/RIszTZEIrjrjEl7v3dQUHxnonWde',
                'email_verified_at' => \Carbon\Carbon::now(),
                'remember_token' => '',
            ],
            [
                'id' => 2,
                'name' => 'Siddharth Jain',
                'email' => 'asifnadaf@gmail.com',
                'password' => '$2y$10$c7Sm1kYMbTiHa0/6M5dhTevpBSxQhjZ6XUbDCnaWf8r1/TLUSdPEu',
                'email_verified_at' => \Carbon\Carbon::now(),
                'remember_token' => null,
            ],
            [
                'id' => 3,
                'name' => 'Jay Yadav',
                'email' => 'asifrnadaf@gmail.com',
                'password' => '$2y$10$c7Sm1kYMbTiHa0/6M5dhTevpBSxQhjZ6XUbDCnaWf8r1/TLUSdPEu',
                'email_verified_at' => \Carbon\Carbon::now(),
                'remember_token' => null,
            ],

        ];

        foreach ($items as $item) {
            \App\User::create($item);
        }
    }
}
