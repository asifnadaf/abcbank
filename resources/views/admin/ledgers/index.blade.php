@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6  col-md-offset-3">
            @include('include.messages')

            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('global.ledger.title')
                </div>

                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-striped {{ count($users) > 0 ? 'datatable' : '' }} ">
                        <thead>
                        <tr>

                            <th>@lang('global.users.fields.name')</th>
                            <th>@lang('global.users.fields.email')</th>
                            <th>&nbsp;</th>

                        </tr>
                        </thead>

                        <tbody>
                        @if (count($users) > 0)
                            @foreach ($users as $user)
                                <tr data-entry-id="{{ $user->id }}">

                                    <td field-key='name'>{{ $user->name }}</td>
                                    <td field-key='email'>{{ $user->email }}</td>
                                    <td>
                                        @can('user_view')
                                            <a href="{{ route('admin.ledgers.show',[$user->id]) }}"
                                               class="btn btn-xs btn-primary">@lang('global.app_view_statement')</a>
                                        @endcan
                                    </td>

                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="10">@lang('global.app_no_entries_in_table')</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
