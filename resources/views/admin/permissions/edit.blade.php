@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6  col-md-offset-3">
            @include('include.messages')
            <h3 class="page-title">@lang('global.permissions.title')</h3>

            {!! Form::model($permission, ['method' => 'PUT', 'route' => ['admin.permissions.update', $permission->id]]) !!}

            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('global.app_edit')
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('title', trans('global.permissions.fields.title').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('title'))
                                <p class="help-block">
                                    {{ $errors->first('title') }}
                                </p>
                            @endif
                        </div>
                    </div>

                </div>
            </div>

            {!! Form::submit(trans('global.app_update'), ['class' => 'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop

