@inject('request', 'Illuminate\Http\Request')
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-6  col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('global.statement.fields.statement-of-account')
                </div>

                <div class="panel-body table-responsive">
                    <table class="table {{ count($ledgers) > 0 ? 'datatable' : '' }}">
                        <thead>
                        <tr>
                            <th>@lang('global.ledger.fields.sr-number')</th>
                            <th>@lang('global.ledger.fields.date-time')</th>
                            <th>@lang('global.ledger.fields.amount')</th>
                            <th>@lang('global.ledger.fields.type')</th>
                            <th>@lang('global.ledger.fields.details')</th>
                            <th>@lang('global.ledger.fields.balance')</th>
                        </tr>
                        </thead>
                        <?php $counter = 0; ?>
                        <tbody>
                        @if (count($ledgers) > 0)
                            @foreach ($ledgers as $ledger)
                                <?php $counter = $counter + 1; ?>
                                <tr data-entry-id="{{ $ledger->id }}">
                                    <td field-key='sr_number'>{{ $counter }}</td>
                                    <td field-key='date_time'>{{ $ledger->date_time }}</td>
                                    <td field-key='amount'>{{ $ledger->amount }}</td>
                                    <td field-key='type'>{{ $ledger->type }}</td>
                                    <td field-key='details'>{{ $ledger->details }}</td>
                                    <td field-key='balance'>{{ $ledger->balance }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="9">@lang('global.app_no_entries_in_table')</td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('javascript')
    <script>

    </script>
@endsection