@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-4  col-md-offset-4">
            @include('include.messages')

            {!! Form::open(['method' => 'POST', 'route' => ['admin.transfers.store']]) !!}

            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('global.transfer.fields.transfer-money')
                </div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('email', trans('global.users.fields.email').'*', ['class' => 'control-label']) !!}
                            {!! Form::email('email', old('email'), ['class' => 'form-control', 'placeholder' => 'Enter email address', 'required' => '']) !!}
                            <p class="help-block"></p>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('amount', trans('global.ledger.fields.amount').'*', ['class' => 'control-label']) !!}
                            {!! Form::text('amount', old('amount'), ['class' => 'form-control', 'placeholder' => 'Enter amount to transfer', 'required' => '']) !!}
                            <p class="help-block">Your a/c balance is {{$currentBalance}}</p>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::submit(trans('global.app_transfer'), ['class' => 'btn btn-primary btn-block']) !!}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    {!! Form::close() !!}
@stop

@section('javascript')
    @parent

    <script src="{{ url('adminlte/plugins/datetimepicker/moment-with-locales.min.js') }}"></script>
    <script src="{{ url('adminlte/plugins/datetimepicker/bootstrap-datetimepicker.min.js') }}"></script>
    <script>
        $(function () {
            moment.updateLocale('{{ App::getLocale() }}', {
                week: {dow: 1} // Monday is the first day of the week
            });

            $('.datetime').datetimepicker({
                format: "{{ config('app.datetime_format_moment') }}",
                locale: "{{ App::getLocale() }}",
                sideBySide: true,
            });

        });
    </script>

@stop