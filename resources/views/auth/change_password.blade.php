@extends('layouts.app')

@section('content')

@section('content')
    <div class="row">
        <div class="col-md-4  col-md-offset-4">
        @include('include.messages')

        {!! Form::open(['method' => 'PATCH', 'route' => ['auth.change_password']]) !!}
        <!-- If no success message in flash session show change password form  -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    @lang('global.app_change_password')
                </div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('current_password', trans('global.app_current_password'), ['class' => 'control-label']) !!}
                            {!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('current_password'))
                                <p class="help-block">
                                    {{ $errors->first('current_password') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('new_password', trans('global.app_new_password'), ['class' => 'control-label']) !!}
                            {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('new_password'))
                                <p class="help-block">
                                    {{ $errors->first('new_password') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::label('new_password_confirmation', trans('global.app_password_confirm'), ['class' => 'control-label']) !!}
                            {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => '']) !!}
                            <p class="help-block"></p>
                            @if($errors->has('new_password_confirmation'))
                                <p class="help-block">
                                    {{ $errors->first('new_password_confirmation') }}
                                </p>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 form-group">
                            {!! Form::submit(trans('global.app_save'), ['class' => 'btn btn-primary btn-block']) !!}
                        </div>
                    </div>

                </div>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@stop

