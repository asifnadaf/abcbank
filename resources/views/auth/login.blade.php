@extends('layouts.auth')

@section('content')

    <style>

        .col-centered {
            float: none;
            margin: 0 auto;
        }

    </style>

    <div class="row" style="padding-top: 60px;">
        <div class="col-xs-8 col-centered">
            <div class="row">


                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="box box-solid" style="box-shadow:none">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="register-logo" style="text-align: left;">
                                            <img src="{{ url('').'/company_logo.png'}}"
                                                 class="img-rounded" style=" height: 41px"
                                                 alt="User Image">
                                        </div>
                                        <div>
                                            <h3 style="color: #467fcf">WELCOME TO ABC BANK</h3>
                                            <p class="mb-5"
                                               style="font-size: 16px;padding-top: 15px;padding-bottom: 15px;color: #333">
                                                ABC Bank began operations in 1995 with a simple mission: to be a
                                                "World-class Indian Bank". We realised that only a single-minded focus
                                                on product quality and service excellence would help us get there.
                                                Today, we are proud to say that we are well on our way towards that
                                                goal.
                                                It is extremely gratifying that our efforts towards providing customer
                                                convenience have been appreciated both nationally and internationally.
                                            </p>
                                        </div>
                                        <div>
                                            <a href="/" class="btn btn-primary mr-2 mb-2">Know
                                                more</a>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>

                <!-- ./col -->
                <div class="col-md-4">
                    <div class="box box-solid" style="box-shadow:none">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    <h3>Login into your account</h3>
                                    <div class="row">
                                        <div class="col-xs-12 form-group">
                                            @foreach (['login-error-danger', 'login-error-warning', 'login-error-success', 'login-error-info'] as $key)
                                                @if(Session::has($key))
                                                    <p class="alert alert-danger">{{ Session::get($key) }} <a href="#"
                                                                                                              class="close"
                                                                                                              data-dismiss="alert"
                                                                                                              aria-label="close">&times;</a>
                                                    </p>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>

                                    <form role="form" method="POST" action="{{ url('/login') }}">
                                        <input type="hidden"
                                               name="_token"
                                               value="{{ csrf_token() }}">

                                        <div class=" has-feedback form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                            <input type="email"
                                                   class="form-control"
                                                   name="email"
                                                   placeholder="Enter email"
                                                   value="{{ old('email') }}">

                                            @if ($errors->has('email'))
                                                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                                            @endif

                                        </div>

                                        <div class=" has-feedback form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                            <input type="password"
                                                   class="form-control"
                                                   placeholder="Enter password"
                                                   name="password">

                                            @if ($errors->has('password'))
                                                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                                            @endif

                                        </div>

                                        <div class="form-group has-feedback">
                                            <label>
                                                <input type="checkbox"
                                                       name="remember"> @lang('global.app_remember_me')
                                            </label>
                                        </div>


                                        <div class="form-group has-feedback">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block btn-flat"> @lang('global.app_login')</button>
                                        </div>
                                        <div class="form-group has-feedback">
                                            <div>
                                                <span style="padding-right: 5px">
                                                Don't have an account?
                                                    <a href="{{ route('auth.register') }}">@lang('global.app_registration')</a>
                                                </span>|
                                                <span style="padding-left: 5px">
                                                    <a href="{{ route('auth.password.reset') }}">@lang('global.app_forgot_password')</a>
                                                </span>

                                            </div>
                                        </div>
                                    </form>
                                </div>


                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


                <!-- ./col -->


            </div>
        </div>
    </div>

@endsection