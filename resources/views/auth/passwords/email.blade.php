
@extends('layouts.auth')

@section('content')

    <style>

        .col-centered{
            float: none;
            margin: 0 auto;
        }

    </style>

    <div class="row" style="padding-top: 60px;">
        <div class="col-xs-8 col-centered">
            <div class="row">


                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-10">
                            <div class="box box-solid" style="box-shadow:none">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="register-logo" style="text-align: left;">
                                            <img src="{{ url('').'/company_logo.png'}}"
                                                 class="img-rounded" style=" height: 41px"
                                                 alt="User Image">
                                        </div>
                                        <div>
                                            <h3 style="color: #467fcf">WELCOME TO ABC BANK</h3>
                                            <p class="mb-5"
                                               style="font-size: 16px;padding-top: 15px;padding-bottom: 15px;color: #333">
                                                ABC Bank began operations in 1995 with a simple mission: to be a "World-class Indian Bank". We realised that only a single-minded focus on product quality and service excellence would help us get there. Today, we are proud to say that we are well on our way towards that goal.
                                                It is extremely gratifying that our efforts towards providing customer convenience have been appreciated both nationally and internationally.
                                            </p>
                                        </div>
                                        <div>
                                            <a href="/" class="btn btn-primary mr-2 mb-2">Know
                                                more</a>
                                        </div>

                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>

                    <!-- /.box -->
                </div>


                <!-- ./col -->
                <div class="col-md-4">
                    <div class="box box-solid" style="box-shadow:none">
                        <div class="box-body">
                            <div class="row">
                                <div class="col-lg-12">

                                    <h3>Reset password</h3>

                                    <form role="form" method="POST" action="{{ url('password/email') }}">
                                        <input type="hidden"
                                               name="_token"
                                               value="{{ csrf_token() }}">
                                        <div class=" has-feedback form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                            <input type="email"
                                                   class="form-control"
                                                   name="email"
                                                   placeholder="Enter email"
                                                   value="{{ old('email') }}" >
                                        </div>
                                        <div class="form-group has-feedback">
                                            <button type="submit"
                                                    class="btn btn-primary btn-block btn-flat"> Reset password
                                            </button>
                                        </div>
                                    </form>

                                    @if (session('status'))
                                        <div class="alert alert-success">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <div class="form-group has-feedback">
                                        <div>
                                            <a href="{{ route('auth.login') }}" class="text-center">Back to login page</a>
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>


                <!-- ./col -->


            </div>
        </div>
    </div>

@endsection