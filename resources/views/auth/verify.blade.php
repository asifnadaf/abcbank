@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-md-6  col-md-offset-3">
            @include('include.messages')

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="card-header">{{ __('Verify Your Email Address') }}</div>
                </div>

                <div class="panel-body table-responsive">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('A fresh verification link has been sent to your email address.') }}
                        </div>
                    @endif

                    {{ __('Before proceeding, please check your email for a verification link.') }}
                    {{ __('If you did not receive the email') }}, <a
                            href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.

                </div>
            </div>
        </div>
    </div>
@endsection
