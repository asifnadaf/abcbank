@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4  col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Welcome {{Auth::user()->name}}
                </div>

                <div class="panel-body table-responsive">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td>@lang('global.home.fields.id')</td>
                            <td field-key='email'>{{Auth::user()->email}}</td>
                        </tr>

                        <tr>
                            <td>@lang('global.home.fields.balance')</td>
                            <td field-key='current_balance'>{{$currentBalance}}</td>
                        </tr>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
