@inject('request', 'Illuminate\Http\Request')
<!-- Left side column. contains the sidebar -->

<aside class="main-sidebar hidden-lg">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <div class="user-panel" style="padding-top: 20px;padding-bottom: 20px">
            <div class="pull-left image">
                        <span class="text-circle-small" style="width: 60px; height: 60px;margin-right: 5px">
                            {{Auth::user()->userNameInitials ? Auth::user()->userNameInitials : ''}}
                        </span>
            </div>
            <div class="pull-left info" style="padding-left: 5px">
                <p>{{Auth::User()->name}}</p>
            </div>
        </div>

        <ul class="sidebar-menu">

            <li class="{{ $request->segment(1) == 'home' ? 'active' : '' }}">
                <a href="{{ url('/') }}">
                    <i class="fa fa-home"></i>
                    <span class="title">@lang('global.app_dashboard')</span>
                </a>
            </li>


            @can('deposit_create')
                <li>
                    <a href="{{ route('admin.deposits.create') }}">
                        <i class="fa fa-cloud-upload"></i>
                        <span>@lang('global.deposit.title')</span>
                    </a>
                </li>@endcan


            @can('withdraw_create')
                <li>
                    <a href="{{ route('admin.withdrawals.create') }}">
                        <i class="fa fa-cloud-download"></i>
                        <span>@lang('global.withdraw.title')</span>
                    </a>
                </li>@endcan


            @can('transfer_create')
                <li>
                    <a href="{{ route('admin.transfers.create') }}">
                        <i class="fa fa-exchange"></i>
                        <span>@lang('global.transfer.title')</span>
                    </a>
                </li>@endcan

            @can('statement_access')
                <li>
                    <a href="{{ route('admin.statements.index') }}">
                        <i class="fa fa-list"></i>
                        <span>@lang('global.statement.title')</span>
                    </a>
                </li>@endcan

            @can('ledger_access')
                <li>
                    <a href="{{ route('admin.ledgers.index') }}">
                        <i class="fa fa-dollar"></i>
                        <span>@lang('global.ledger.title')</span>
                    </a>
                </li>@endcan

            @can('user_management_access')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-users"></i>
                        <span>@lang('global.user-management.title')</span>
                        <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        @can('permission_access')
                            <li>
                                <a href="{{ route('admin.permissions.index') }}">
                                    <i class="fa fa-briefcase"></i>
                                    <span>@lang('global.permissions.title')</span>
                                </a>
                            </li>@endcan

                        @can('role_access')
                            <li>
                                <a href="{{ route('admin.roles.index') }}">
                                    <i class="fa fa-briefcase"></i>
                                    <span>@lang('global.roles.title')</span>
                                </a>
                            </li>@endcan

                        @can('user_access')
                            <li>
                                <a href="{{ route('admin.users.index') }}">
                                    <i class="fa fa-user"></i>
                                    <span>@lang('global.users.title')</span>
                                </a>
                            </li>@endcan

                    </ul>
                </li>@endcan


            <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                <a href="{{ route('auth.change_password') }}">
                    <i class="fa fa-key"></i>
                    <span class="title">@lang('global.app_change_password')</span>
                </a>
            </li>

            <li>
                <a href="#logout" onclick="$('#logout').submit();">
                    <i class="fa fa-arrow-left"></i>
                    <span class="title">@lang('global.app_logout')</span>
                </a>
            </li>
        </ul>
    </section>
</aside>

