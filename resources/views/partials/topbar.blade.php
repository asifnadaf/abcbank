@inject('request', 'Illuminate\Http\Request')

<header class="main-header  header-border-bottom">
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">

        <div class="row hidden-lg">
            <div class=" col-xs-1 ">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
            </div>
            <div class="col-xs-11">
                <div class="container-fluid ">
                        <ul class="nav navbar-nav ">
                            <li><a href="{{ url('/admin/home') }}"
                                   style="padding: 5px;">
                                    <div>
                                        <img src="{{ url('').'/company_logo.png'}}"
                                              style=" height: 41px">
                                    </div>
                                </a>
                            </li>
                        </ul>
                </div>
            </div>
        </div>


        <div class="row hidden-xs hidden-sm hidden-md">
            <div class="col-xs-3">
                <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li><a href="{{ url('/admin/home') }}"
                               style="padding: 5px;">
                                <div style="text-align: left;">
                                    <img src="{{ url('').'/company_logo.png'}}"
                                          style=" height: 41px">
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="container-fluid">
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">

                            <li class="{{ $request->segment(2) == 'home' ? 'active' : '' }}">
                                <a href="{{ url('/') }}">
                                    <i class="fa fa-home"></i>
                                    <span class="title">@lang('global.app_dashboard')</span>
                                </a>
                            </li>


                            @can('deposit_create')
                                <li class="{{ $request->segment(2) == 'deposits' ? 'active' : '' }}">
                                    <a href="{{ route('admin.deposits.create') }}">
                                        <i class="fa fa-cloud-upload"></i>
                                        <span>@lang('global.deposit.title')</span>
                                    </a>
                                </li>@endcan


                            @can('withdraw_create')
                                <li class="{{ $request->segment(2) == 'withdrawals' ? 'active' : '' }}">
                                    <a href="{{ route('admin.withdrawals.create') }}">
                                        <i class="fa fa-cloud-download"></i>
                                        <span>@lang('global.withdraw.title')</span>
                                    </a>
                                </li>@endcan


                            @can('transfer_create')
                                <li class="{{ $request->segment(2) == 'transfers' ? 'active' : '' }}">
                                    <a href="{{ route('admin.transfers.create') }}">
                                        <i class="fa fa-exchange"></i>
                                        <span>@lang('global.transfer.title')</span>
                                    </a>
                                </li>@endcan

                            @can('statement_access')
                                <li class="{{ $request->segment(2) == 'statements' ? 'active' : '' }}">
                                    <a href="{{ route('admin.statements.index') }}">
                                        <i class="fa fa-list"></i>
                                        <span>@lang('global.statement.title')</span>
                                    </a>
                                </li>@endcan

                            @can('ledger_access')
                                <li class="{{ $request->segment(2) == 'ledgers' ? 'active' : '' }}">
                                    <a href="{{ route('admin.ledgers.index') }}">
                                        <i class="fa fa-dollar"></i>
                                        <span>@lang('global.ledger.title')</span>
                                    </a>
                                </li>@endcan


                            @can('user_management_access')
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle"
                                       data-toggle="dropdown">@lang('global.user-management.title') <span
                                                class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">

                                        @can('permission_access')
                                            <li class="{{ $request->segment(2) == 'permissions' ? 'active' : '' }}">
                                                <a href="{{ route('admin.permissions.index') }}">
                                                    <i class="fa fa-briefcase"></i>
                                                    <span>@lang('global.permissions.title')</span>
                                                </a>
                                            </li>@endcan

                                        @can('role_access')
                                            <li class="{{ $request->segment(2) == 'roles' ? 'active' : '' }}">
                                                <a href="{{ route('admin.roles.index') }}">
                                                    <i class="fa fa-briefcase"></i>
                                                    <span>@lang('global.roles.title')</span>
                                                </a>
                                            </li>@endcan

                                        @can('user_access')
                                            <li class="{{ $request->segment(2) == 'users' ? 'active' : '' }}">
                                                <a href="{{ route('admin.users.index') }}">
                                                    <i class="fa fa-user"></i>
                                                    <span>@lang('global.users.title')</span>
                                                </a>
                                            </li>@endcan
                                    </ul>
                                </li>
                            @endcan
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"
                               aria-expanded="true">

                                <div class="text-center">

                                    <div class="table no-border no-margin">
                                        <tr>
                                            <td>
                                                <span class="text-circle-small" style="margin-right: 10px">
                                                    {{Auth::user()->userNameInitials ? Auth::user()->userNameInitials : ''}}
                                                </span>
                                            </td>
                                            <td class="text-primary-color"> {{Auth::User()->name}}<span
                                                        class="caret"></span>
                                            </td>
                                        </tr>
                                    </div>
                                </div>

                            </a>
                            <ul class="dropdown-menu dropdown-menu-right">

                                <li class="{{ $request->segment(1) == 'change_password' ? 'active' : '' }}">
                                    <a href="{{ route('auth.change_password') }}">
                                        <i class="fa fa-key"></i>
                                        <span class="title">@lang('global.app_change_password')</span>
                                    </a>
                                </li>
                                <li class="divider"></li>

                                <li>
                                    <a href="#logout" onclick="$('#logout').submit();">
                                        <i class="fa fa-arrow-left"></i>
                                        <span class="title">@lang('global.app_logout')</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    </nav>
</header>





