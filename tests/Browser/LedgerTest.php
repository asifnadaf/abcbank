<?php

namespace Tests\Browser;

use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class LedgerTest extends DuskTestCase
{

    public function testCreateLedger()
    {
        $admin = \App\User::find(1);
        $ledger = factory('App\Ledger')->make();


        $this->browse(function (Browser $browser) use ($admin, $ledger) {
            $browser->loginAs($admin)
                ->visit(route('admin.ledgers.index'))
                ->clickLink('Add new')
                ->type("amount", $ledger->amount)
                ->press('Save')
                ->assertRouteIs('admin.ledgers.index')
                ->assertSeeIn("tr:last-child td[field-key='amount']", $ledger->amount)
                ->logout();
        });
    }

    public function testEditLedger()
    {
        $admin = \App\User::find(1);
        $ledger = factory('App\Ledger')->create();
        $ledger2 = factory('App\Ledger')->make();


        $this->browse(function (Browser $browser) use ($admin, $ledger, $ledger2) {
            $browser->loginAs($admin)
                ->visit(route('admin.ledgers.index'))
                ->click('tr[data-entry-id="' . $ledger->id . '"] .btn-info')
                ->press('Update')
                ->assertRouteIs('admin.ledgers.index')
                ->logout();
        });
    }

    public function testShowLedger()
    {
        $admin = \App\User::find(1);
        $ledger = factory('App\Ledger')->create();


        $this->browse(function (Browser $browser) use ($admin, $ledger) {
            $browser->loginAs($admin)
                ->visit(route('admin.ledgers.index'))
                ->click('tr[data-entry-id="' . $ledger->id . '"] .btn-primary')
                ->assertSeeIn("td[field-key='date_time']", $ledger->date_time)
                ->assertSeeIn("td[field-key='amount']", $ledger->amount)
                ->assertSeeIn("td[field-key='type']", $ledger->type)
                ->assertSeeIn("td[field-key='details']", $ledger->details)
                ->assertSeeIn("td[field-key='created_by']", $ledger->created_by->name)
                ->assertSeeIn("td[field-key='balance']", $ledger->balance)
                ->logout();
        });
    }

}
